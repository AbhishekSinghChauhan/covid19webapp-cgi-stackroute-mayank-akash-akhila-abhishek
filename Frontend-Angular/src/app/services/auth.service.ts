import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private baseUrl:string="https://localhost:7069/api/User/"
  apiurl = "https://localhost:7264/api/WatchList/add";
  apiurl2 = "https://localhost:7264/api/WatchList/";
  
  constructor(private http:HttpClient){}
  signUp(userObj:any){
    return this.http.post<any>(`${this.baseUrl}register`,userObj);
   }
   login(loginObj:any){
     return this.http.post<any>(`${this.baseUrl}authenticate`,loginObj);
   } 
   addWatchlist(watchlistinfo:Array<any>){
    return this.http.post(this.apiurl,{
      userName: watchlistinfo[0],
      country: watchlistinfo[1],
      confirmed: watchlistinfo[2],
      recovered: watchlistinfo[3],
      deaths: watchlistinfo[4],

    },{responseType:'text',}); 
  }
  signOut(){
    sessionStorage.clear();
  }
  updateUser(updateInfo: Array<any>){
    return this.http.post(this.baseUrl + 'UpdateUser',{
      Email: updateInfo[0],
      Password: updateInfo[1]
    } ,{responseType: 'text',});
  }
  getUserData(){
    return this.http.get(this.apiurl2 + sessionStorage.getItem('username') );
  }
}
