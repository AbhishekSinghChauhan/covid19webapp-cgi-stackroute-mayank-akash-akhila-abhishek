import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-watchlist',
  templateUrl: './watchlist.component.html',
  styleUrls: ['./watchlist.component.css']
})
export class WatchlistComponent implements OnInit {
  userData:any = [];
  username  = sessionStorage.getItem('username');
  

  constructor(private getwatch : AuthService) { 
    this.getwatch.getUserData().subscribe(data =>{
      console.log(data);
      this.userData = data;
    })
  }
  

  ngOnInit(): void {
  }

}
