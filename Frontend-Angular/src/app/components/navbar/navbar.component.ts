import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  menuType: String = 'default';
  constructor(private route: Router) { }

  checkLogout(){
    return sessionStorage.getItem("username")? true:false;
  }
  checkLogin(){
    return sessionStorage.getItem("username")? false:true;
  }

  ngOnInit(): void {
    this.route.events.subscribe((val:any)=>{
      if(val.url){
        if(sessionStorage.getItem('username') && val.url.includes('username')){
          this.menuType = "login";
        }
        else{
          this.menuType = 'default';
        }
      }
    })
  }

}
