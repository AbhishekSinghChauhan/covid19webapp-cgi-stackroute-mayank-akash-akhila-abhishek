﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace WatchListAPI.Models
{
    [BsonIgnoreExtraElements]
    public class WatchList
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; } = String.Empty;

        [BsonElement("username")]
        public string UserName { get; set; } = String.Empty;

        [BsonElement("country")]
        public string Country { get; set; } = String.Empty;

        [BsonElement("confirmed")]
        public int Confirmed { get; set; }

        [BsonElement("recovered")]
        public int Recovered { get; set; }

        [BsonElement("deaths")]
        public int Deaths { get; set; }
    }
}
